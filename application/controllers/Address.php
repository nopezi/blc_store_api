<?php 

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Address extends REST_Controller {

	public function index_get()
	{

		$user_id = $this->get('user_id');

		$data = $this->db->get_where('wpaf_wc_customer_lookup', ['user_id'=>$user_id])->result();

		$this->response([
			'status'  => true,
			'message' => 'Berhasil mendapatkan data alamat user',
			'data'    => $data
		], 200);
		
	}

}

/* End of file Address.php */
/* Location: ./application/controllers/Address.php */