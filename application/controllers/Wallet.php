<?php 

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Wallet extends REST_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_wallet');
	}

	public function index_get()
	{

		$id_user = $this->get('id_user');
		$type    = $this->get('type'); 

		$total = $this->m_wallet->total_dana($id_user, $type);
		$total = !empty($total->total)?$total->total:0;
		$riwayat_dana = $this->m_wallet->riwayat_dana($id_user, $type);

		if (!empty($riwayat_dana)) {
			
			$this->response([
				'status'  => true,
				'message' => 'Data e-wallet berhasil didapat',
				'dana_total' => 'Rp.'.number_format($total),
				'data'    => $riwayat_dana
			], 200);

		} else {

			$this->response([
				'status'  => true,
				'message' => 'Data e-wallet berhasil didapat',
				'dana_total' => 'Rp.'.number_format($total),
				'data'    => []
			], 200);

		}
		
	}

	public function total_get()
	{

		$id_user = $this->get('id_user');
		$type    = $this->get('type'); 

		if (empty($type)) {
			$total = $this->m_wallet->total_dana($id_user, $type);
			$total = !empty($total->total)?$total->total:0;
		} else if (strtolower($type) == 'out') {
			$total = $this->m_wallet->total_dana_out($id_user, $type);
			$total = !empty($total->total)?($total->total):0;
		} else {
			$total = $this->m_wallet->total_dana_in($id_user, $type);
			// $total = !empty($total->total)?$total->total:0;
		}

		$this->response([
			'status'  => true,
			'message' => 'Data Total e-wallet berhasil didapat',
			'data'    => 'Rp.'.number_format($total)
		], 200);
		
	}

	public function riwayat_dana_get()
	{

		$id_user = $this->get('id_user');

		if (empty($id_user)) {
			$this->response([
				'status'  => false,
				'message' => 'Id user wajib di isi',
				'data'    => []
			], 200);
		}

		$riwayat_dana = $this->m_wallet->riwayat_dana_v2($id_user);

		if (!empty($riwayat_dana)) {
			
			$this->response([
				'status'  => true,
				'message' => 'Data riwayat e-wallet berhasil didapat',
				'data'    => $riwayat_dana
			], 200);

		} else {

			$this->response([
				'status'  => false,
				'message' => 'Belum ada data riwayat transaksi',
				'data'    => []
			], 200);

		}

	}

}

/* End of file Wallet.php */
/* Location: ./application/controllers/Wallet.php */