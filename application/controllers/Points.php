<?php 

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Points extends REST_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_points');
		$this->load->model('m_wallet');
	}

	public function index()
	{
		
	}

	public function recordpoints_get()
	{

		$id_user = $this->get('id_user');

		if (!empty($id_user)) {
			$hasil = $this->m_points->recordpoints_v2($id_user);
		} else {
			$hasil = $this->m_points->recordpoints($id_user);
		}
		

		if (!empty($hasil)) {
			
			$this->response([
				'status'  => true,
				'message' => $hasil['pesan'],
				// 'total_earned' => $total,
				'data'    => $hasil['hasil']
			], 200);

		} else {

			$this->response([
				'status'  => false,
				'message' => 'Data points tidak ada',
				'data'    => []
			], 200);

		}

	}

	public function riwayat_get()
	{

		$id_user = $this->get('id_user');
		$tab	 = $this->get('tab');

		if (empty($tab)) {
			$this->response([
				'status'  => false,
				'message' => 'tab tidak boleh kosong',
				'data'    => []
			],200);
		}

		$data = $this->m_points->riwayat($id_user, $tab);

		if (!empty($data)) {
			
			$this->response([
				'status'      => true,
				'message'     => 'Data riwayat point berhasil didapat',
				'total_point' => $data['total_point'],
				'data'        => $data['riwayat_nilai']
			], 200);

		} else {

			$this->response([
				'status'      => true,
				'message'     => 'Data riwayat point berhasil didapat',
				'total_point' => 0,
				'data'        => []
			], 200);

		}

	}

	public function all_riwayat_get()
	{

		$id_user = $this->get('id_user');

		if (empty($id_user)) {
			
			$this->response([
				'status'  => false,
				'message' => 'id user tidak boleh kosong',
				'data'    => []
			], 200);

		}

		$data = $this->m_points->semua_riwayat($id_user);

		if (!empty($data)) {

			$this->response([
				'status'  => true,
				'message' => 'data semua riwayat point berhasil didapat',
				'data'    => $data
			], 200);

		} else {

			$this->response([
				'status'  => false,
				'message' => 'data semua riwayat point gagal didapat',
				'data'    => []
			], 200);

		}

	}

	public function potongan_point_post()
	{

		$user_id  = $this->post('user_id');
		$order_id = $this->post('order_id');
		$point    = $this->post('point');
		$wallet   = $this->post('wallet');

		$totalpoints = $this->db->where('userid', $user_id)
								->order_by('id', 'desc')
								->get('wpaf_rsrecordpoints')
								->first_row();
		$totalpoints = $totalpoints->totalpoints - $point;

		if ($totalpoints < 0) {
			$totalpoints = 0;
		}

		$insert_point = [
			'earnedpoints' => '0',
			'redeempoints' => $point,
			'userid'       => $user_id,
			'expirydate'   => '999999999999',
			'checkpoints'  => 'RP',
			'redeemequauivalentamount' => $point,
			'orderid'       => $order_id,
			'totalpoints'   => $totalpoints,
			'showmasterlog' => '0',
			'showuserlog'   => '0',
			'productid'     => '0'
		];

		$update_point = $this->m_points->tambah($insert_point);

		$insert_wallet = [
			'user_id'  => $user_id,
			'order_id' => $order_id,
			'value'    => $wallet,
			'type'     => 'out',
			'label'	   => 'wallet-use',
			'meta_data'=> 'a:0:{}',
			'valid_point' => '1',
			'refundable'  => '0',
			'created_at'  => date('Y-m-d H:i:s')
		];

		$update_wallet = $this->m_wallet->tambah($insert_wallet);

		$this->response([
			'status'  => true,
			'message' => 'Berhasil update point wallet',
			'data'    => [
				'points' => $update_point,
				'wallet' => $update_wallet
			]
		], 200);

	}

}

/* End of file Points.php */
/* Location: ./application/controllers/Points.php */