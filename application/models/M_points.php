<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_points extends CI_Model {

	public function riwayat($id_user, $tab)
	{

		$belakang = '+';
		$judul = 'Point baru dari pesanan';
		if (!empty($tab) && $tab == 'pembelanjaan') {
			$belakang = '-';
			$judul = 'Point yang anda tukarkan untuk pesanan';
		}

		$total_point = null;

		if (!empty($id_user)) {
			$total_point = $this->db->select('totalpoints')
									->where('userid',$id_user)
									->order_by('id', 'desc')
									->get('wpaf_rsrecordpoints')
									->first_row();
		}

		$this->db->select('earnedpoints, redeempoints, orderid, refuserid')
				 ->from('wpaf_rsrecordpoints wpr')
				 ->where('userid', $id_user);

		if ($tab == 'pembelanjaan') {
			$this->db->where('redeempoints !=', '0');
		} else {
			$this->db->where('earnedpoints !=', '0');
		}

		$d_riwayat_nilai = $this->db->get()->result();

		if (!empty($d_riwayat_nilai)) {
			
			foreach ($d_riwayat_nilai as $key => $value) {

				$value->judul = $judul;

				$value->redeempoints = !empty($value->redeempoints)?'-'.$value->redeempoints:"0";
				$value->earnedpoints = !empty($value->earnedpoints)?'+'.$value->earnedpoints:"0";
				$value->orderid = '#'.$value->orderid;

				if (!empty($tab) && $tab == 'pembelanjaan') {
					unset($value->earnedpoints);
				} else if(!empty($tab) && $tab == 'penghasilan') {
					unset($value->redeempoints);
				}

			}

		}

		return [
			'riwayat_nilai' => $d_riwayat_nilai,
			'total_point'   => !empty($total_point->totalpoints)?:0
		];

	}

	public function recordpoints($id_user)
	{

		if (!empty($id_user)) {
			$user = $this->db->get_where('wpaf_users', ['id'=>$id_user])->result();
		} else {
			$user = $this->db->get('wpaf_users')->result();
		}

		/*
		SELECT 
		earnedpoints,
		redeempoints,
		earnedequauivalentamount,
		totalpoints
		FROM `wpaf_rsrecordpoints` WHERE userid = 1
		*/

		if (!empty($user)) {
			
			foreach ($user as $key => $value) {

				$curent_data = $this->db->select('totalpoints')
										->where('userid',$value->ID)
										->order_by('id', 'desc')
										->get('wpaf_rsrecordpoints')
										->first_row();

				$total = $this->db->select('SUM(wpr.earnedpoints) as total_earned, 
											SUM(wpr.redeempoints) as total_redeemed,
											SUM(wpr.earnedequauivalentamount) as tes1')
								 ->from('wpaf_rsrecordpoints wpr')
								 ->where('wpr.userid', $value->ID)
								 ->order_by('wpr.id', 'desc')
								 ->get()
								 ->first_row();
				$hasil[] = [
					'id_user'			   => $value->ID,
					'email'                => $value->user_email,
					'total_earnedpoints'   => $total->total_earned?:0,
					'total_redeemedpoints' => $total->total_redeemed?:0,
					'current_available'    => !empty($curent_data->totalpoints)?:0,
				];
			}

		}

		$pesan = 'Berhasil mendapatkan data Points';
		if (!empty($id_user) && !empty($user)) {
			$pesan = 'Berhasil mendapatkan data Points '. $user[0]->user_nicename;
		}

		return [
			'hasil' => $hasil,
			'pesan' => $pesan
		];

	}

	public function recordpoints_v2($id_user)
	{

		if (!empty($id_user)) {

			$user = $this->db->get_where('wpaf_users', ['id'=>$id_user])->first_row();

			$curent_data = $this->db->select('totalpoints')
										->where('userid',$id_user)
										->order_by('id', 'desc')
										->get('wpaf_rsrecordpoints')
										->first_row();

			$total = $this->db->select('wpr.earnedpoints as total_earned, 
										wpr.redeempoints as total_redeemed')
							 ->from('wpaf_rsrecordpoints wpr')
							 ->where('wpr.userid', $id_user)
							 ->order_by('wpr.id', 'desc')
							 ->get()
							 ->first_row();

			$total_earned = $this->db->select_sum('earnedpoints')
									 ->from('wpaf_rsrecordpoints')
									 ->where('userid', $id_user)
									 ->order_by('id', 'desc')
									 ->get()
									 ->first_row();

			$total_redeemed = $this->db->select_sum('redeempoints')
									 ->from('wpaf_rsrecordpoints')
									 ->where('userid', $id_user)
									 ->order_by('id', 'desc')
									 ->get()
									 ->first_row();

			// echo "<pre>";
			// print_r ($total_redeemed);
			// echo "</pre>";
			// die();

			$total_earned = !empty($total_earned->earnedpoints)?$total_earned->earnedpoints:0;
			$total_redeemed = !empty($total_redeemed->redeempoints)?$total_redeemed->redeempoints:0;
			
			$hasil[] = [
				'id_user'			   => $id_user,
				'email'                => $user->user_email,
				'total_earnedpoints'   => $total_earned,//$total->total_earned?:0,
				'total_redeemedpoints' => $total_redeemed,//$total->total_redeemed?:0,
				'current_available'    => !empty($curent_data->totalpoints)?$curent_data->totalpoints:0,
			];

		}

		$pesan = 'Berhasil mendapatkan data Points';
		if (!empty($id_user) && !empty($user)) {
			$pesan = 'Berhasil mendapatkan data Points '. $user->user_nicename;
		}

		return [
			'hasil' => $hasil,
			'pesan' => $pesan
		];

	}


	public function semua_riwayat($id_user)
	{

		$d_riwayat_nilai = $this->db->select('earnedpoints, redeempoints, orderid, refuserid')
									 ->from('wpaf_rsrecordpoints wpr')
									 ->where('userid', $id_user)
									 ->get()
									 ->result();

		$data = null;
		if (!empty($d_riwayat_nilai)) {
			
			foreach ($d_riwayat_nilai as $key => $dwn) {
			
				if (!empty($dwn->earnedpoints)) {
					
					$data[] = [
						'judul'        => 'Point baru dari pesanan',
						'redeempoints' => '0',
						'earnedpoints' => '+'.$dwn->earnedpoints,
						'orderid'      => '#'.$dwn->orderid,
						'refuserid'    => $dwn->refuserid
					];

				}

				if (!empty($dwn->redeempoints)) {

					$data[] = [
						'judul'        => 'Point yang anda tukarkan untuk pesanan',
						'redeempoints' => '-'.$dwn->redeempoints,
						'earnedpoints' => '0',
						'orderid'      => '#'.$dwn->orderid,
						'refuserid'    => $dwn->refuserid
					];

				}
				
	 
			}

		}

		return $data;

	}

	public function tambah($insert)
	{

		$this->db->insert('wpaf_rsrecordpoints', $insert);
		$success = $this->db->affected_rows();

		if($success){

			$insert_id = $this->db->insert_id();
			$data = $this->db->get_where('wpaf_rsrecordpoints',['id' => $insert_id])->first_row();
			return $data;

		}

	}	

}

/* End of file M_points.php */
/* Location: ./application/models/M_points.php */