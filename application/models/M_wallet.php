<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_wallet extends CI_Model {

	public function total_dana_in($id_user, $type=null)
	{

		$data_out = $this->db->where('user_id', $id_user)
							 ->where('valid_point', '1')
							 ->where('type', 'out')
							 ->where('refundable', '0')
							 ->where('label', 'wallet-use')
							 ->order_by('id', 'desc')
							 ->get('wpaf_sejowoo_wallets')
							 ->first_row();

		$data_in = $this->db->where('user_id', $id_user)
							->where('type', 'in')
							->order_by('id', 'desc')
							->get('wpaf_sejowoo_wallets')
							->first_row();

		$out = !empty($data_out->value)?$data_out->value:0;
		$in  = !empty($data_in->value)?$data_in->value:0;

		# jumlah wallet terakhir yang masuk di kurangi jumlah wallet terakhir yang keluar
        return $in - $out;

	}

	public function total_dana_out($id_user, $type=null)
	{

		$data_out = $this->db->select('sum(value) as total')
							 ->where('user_id', $id_user)
							 ->where('valid_point', '1')
							 ->where('type', 'out')
							 ->where('refundable', '0')
							 ->where('label', 'wallet-use')
							 ->order_by('id', 'desc')
							 ->get('wpaf_sejowoo_wallets')
							 ->first_row();

        return $data_out;
    }

	public function total_dana($id_user, $type=null)
	{

		$this->db->select('SUM(value) as total')
				 ->where('valid_point', '1');

		if (!empty($id_user)) {
			$this->db->where('user_id', $id_user);
		}

		if (!empty($type)) {
			$this->db->where('type', $type);

			if ($type == 'OUT') {
				$this->db->where('refundable', '0')->where('label', 'wallet-use');
			} else {
				$this->db->where('refundable', '1')
						 ->where('label', 'commission')
						 ->or_where('type', $type)
						 ->where('label', 'cashback')
						 ->where('refundable', '0')
						 ->where('valid_point', '1');
			}

		}

		if (!empty($id_user)) {
			$this->db->where('user_id', $id_user);
		}
				 
		# data total
		return $this->db->get('wpaf_sejowoo_wallets')->first_row();

	}

	public function riwayat_dana_v2($id_user)
	{
		
		$data = $this->db->where('user_id', $id_user)
						 ->where('valid_point', 1)
						 ->order_by('id', 'desc')
						 ->get('wpaf_sejowoo_wallets')
						 ->result();

		$riwayat_dana = [];
		foreach ($data as $key => $value) {
			if ($value->type == 'out') {
				if ($value->label = 'wallet-use' && $value->refundable == 0) {
					$riwayat_dana[] = [
						'judul' => 'Penggunaan dana untuk order',
						'order_id' => '#'.$value->order_id,
						'value' => "- Rp.".number_format($value->value),
						'type'  => "OUT"
					];
				}
			} else {
				if (
					$value->label == 'commission' && $value->refundable == 1 ||
					$value->label == 'cashback' && $value->refundable == 0 
				) {
					$riwayat_dana[] = [
						'judul' => 'Komisi Baru dari pesanan',
						'order_id' => '#'.$value->order_id,
						'value' => "+ Rp.".number_format($value->value),
						'type'  => "IN"
					];
				}
			}
		}

		return $riwayat_dana;

	}

	public function riwayat_dana($id_user = null, $type = null)
	{

		$this->db->select('value, order_id, type')
				 ->where('valid_point', '1');

		if (!empty($id_user)) {
			$this->db->where('user_id', $id_user);
		}

		if (!empty($type)) {

			$this->db->where('type', $type);

			if ($type == 'OUT') {
				$this->db->where('refundable', '0')
						 ->where('label', 'wallet-use');
			} else {
				$this->db->where('refundable', '1')
						 ->where('label', 'commission')
						 ->or_where('type', $type)
						 ->where('label', 'cashback')
						 ->where('refundable', '0')
						 ->where('valid_point', '1');
			}

		}

		if (!empty($id_user)) {
			$this->db->where('user_id', $id_user);
		}

		$hasil = $this->db->get('wpaf_sejowoo_wallets')->result();

		if (!empty($hasil)) {
			
			foreach ($hasil as $key => $value) {
				$value->order_id = '#'.$value->order_id;
				if ($value->type == 'in') {
					$value->judul = 'Komisi Baru dari pesanan';//.$value->order_id;
					$value->value = '+ Rp.'.number_format($value->value);
				} else {
					$value->judul = 'Penggunaan dana untuk order';//.$value->order_id;
					$value->value = '- Rp.'.number_format($value->value);
				}
			}

		}

		return $hasil;

	}

	public function tambah($insert)
	{

		$this->db->insert('wpaf_sejowoo_wallets', $insert);
		$success = $this->db->affected_rows();

		if($success){

			$insert_id = $this->db->insert_id();
			$data = $this->db->get_where('wpaf_sejowoo_wallets',['id' => $insert_id])->first_row();
			return $data;

		}

	}	

}

/* End of file M_wallet.php */
/* Location: ./application/models/M_wallet.php */